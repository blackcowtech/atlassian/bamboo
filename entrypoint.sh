#!/usr/bin/env bash

set -eo pipefail

if [ "$1" != "bamboo" ]
then
    exec su ${APP_USER} -c "$@"
fi

# remove dummy bamboo argument
ARGS="${@:2}"

# reset tomcat connector configuration
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8085']/@proxyName" "${BAMBOO_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8085']/@proxyPort" "${BAMBOO_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8085']/@scheme" "${BAMBOO_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8085']/@secure" "${BAMBOO_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --update "//Connector[@port='8085']/@redirectPort" --value "8443" "${BAMBOO_INSTALL}/conf/server.xml"

# The HTTPS connector contains deprecated attributes resulting in:
# SSLHostConfig attribute certificateFile must be defined when using an SSL connector bamboo, as per:
# https://stackoverflow.com/questions/42135892/tomcat-8-5-server-xml-multiple-sslhostconfig-elements-were-provided-for-the-ho/45496152#45496152
# As any HTTPS connections will be terminated at the proxy, this connector will never be used so can be removed
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8443']" "${BAMBOO_INSTALL}/conf/server.xml"

if [ -n "${X_PROXY_NAME}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8085']" --type "attr" --name "proxyName" --value "${X_PROXY_NAME}" "${BAMBOO_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PROXY_PORT}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8085']" --type "attr" --name "proxyPort" --value "${X_PROXY_PORT}" "${BAMBOO_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PROXY_SCHEME}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8085']" --type "attr" --name "scheme" --value "${X_PROXY_SCHEME}" "${BAMBOO_INSTALL}/conf/server.xml"
fi

if [ "${X_PROXY_SCHEME}" = "https" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8085']" --type "attr" --name "secure" --value "true" "${BAMBOO_INSTALL}/conf/server.xml"
    xmlstarlet edit --inplace --pf --ps --update "//Connector[@port='8085']/@redirectPort" --value "${X_PROXY_PORT}" "${BAMBOO_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PATH}" ]
then
    xmlstarlet edit --inplace --pf --ps --update '//Context/@path' --value "${X_PATH}" "${BAMBOO_INSTALL}/conf/server.xml"
fi

DOCKER_SOCKET=/var/run/docker.sock

if [ -S ${DOCKER_SOCKET} ]; then
    # the group name corresponding to the GID for the docker group on the host
    GROUP_NAME=$(stat -c '%G' ${DOCKER_SOCKET})

    if [ "${GROUP_NAME}" = "UNKNOWN" ]
    then
        GID=$(stat -c '%g' ${DOCKER_SOCKET})
        GROUP_NAME="host-docker"
        addgroup --gid ${GID} ${GROUP_NAME}
    fi

    usermod -aG ${GROUP_NAME} ${APP_USER}
fi

exec su ${APP_USER} -c "${BAMBOO_INSTALL}/bin/start-bamboo.sh -fg ${ARGS}"
