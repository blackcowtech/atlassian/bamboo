# Atlassian Bamboo Docker Image

| Application | Current Version |
|-------------|-----------------|
| Bamboo | 6.10.4 |

The image is built on [blackcowtech/atlassian-base:latest](https://gitlab.com/blackcowtech/atlassian/base), which in turn is built on [tomcat:9-jdk8](https://hub.docker.com/_/tomcat).

Other Dockerized Atlassian applications are also available:

* [blackcowtech/atlassian-bitbucket](https://gitlab.com/blackcowtech/atlassian/bitbucket)

Much of the inspiration for these Dockerfiles has come from [blacklabelops](https://github.com/blacklabelops) and [cptactionhank](https://github.com/cptactionhank).
