FROM blackcowtech/atlassian-base:latest
LABEL maintainer="Paul Jefferson <paul.jefferson@blackcow-technology.co.uk>"

ENV \
  BAMBOO_HOME=${APP_HOME}/bamboo \
  BAMBOO_INSTALL=${APP_INSTALL}/bamboo

EXPOSE 8085

RUN set -ex \
  # install docker as per https://docs.docker.com/install/linux/docker-ce/debian/
  && apt-get update \
  && apt-get install -y --no-install-recommends \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg2 \
  software-properties-common \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
  docker-ce \
  docker-ce-cli \
  containerd.io \
  && usermod -aG docker $APP_USER

RUN set -ex \
  # install node.js as per https://github.com/nodesource/distributions/blob/master/README.md#debinstall
  && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
  && apt-get install -y --no-install-recommends \
  nodejs

RUN set -ex \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
  # run deps
  python3 \
  python3-pip \
  python3-setuptools \
  python3-venv \
  virtualenv \
  xmlstarlet \
  && pip3 install --no-cache --upgrade \
  # bamboo dependencies from pip3
  awscli \
  docker-compose

ARG BAMBOO_VERSION

# from https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-${BAMBOO_VERSION}.tar.gz
COPY atlassian-bamboo-${BAMBOO_VERSION}.tar.gz /tmp/

RUN set -ex \
  && mkdir -p ${BAMBOO_HOME} \
  && mkdir -p ${BAMBOO_INSTALL} \
  && usermod -d ${BAMBOO_HOME} ${APP_USER} \
  && tar xzf /tmp/atlassian-bamboo-${BAMBOO_VERSION}.tar.gz -C ${BAMBOO_INSTALL} --strip-components=1 \
  && chown -R ${APP_USER}:${APP_GROUP} ${BAMBOO_HOME} \
  && chown -R ${APP_USER}:${APP_GROUP} ${BAMBOO_INSTALL}

WORKDIR ${BAMBOO_HOME}
VOLUME ["${BAMBOO_HOME}"]

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["bamboo"]
